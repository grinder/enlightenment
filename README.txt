Versão do Enlightenment : 0.24.2
Source do Enlightenment : https://www.enlightenment.org/

Requerimentos:
- slackware64 current
- pacote de audio : https://www.gitlab.com/grinder/audio.git

Conteúdo:
- ecrire
- edi
- emprint
- ephoto
- equate
- evisum
- extra
- terminology

Esse source foi construído principalmente para quem quiser usar a última versão do Enlightenment.

Essa minha versão do Enlightenment é baseada na versão do GArik
https://github.com/GArik/enlightenment-SlackBuilds
***Todos os créditos vão para ele.

Créditos ao Ryan também, obrigado.
https://github.com/ryanpcmcquen/slackENLIGHTENMENT

Como instalar:
- você precisa clonar com o comando : git clone https://gitlab.com/grinder/enlightenment.git
E então dentro da pasta criada execute o script de instalação
sh build-enlightenment.sh

------------------------------------------------------------------------------

Enlightenment version: 0.24.2
Enlightenment source : https://www.enlightenment.org/

Requirements:
- slackware64 current
- audio package : https://www.gitlab.com/grinder/audio.git

Contents:
- ecrire
- edi
- emprint
- ephoto
- equate
- evisum
- extra
- terminology

This source was built primarily for those who want to use the latest version of Enlightenment

This my version of Enlightenment is based on the version of Garik
https://github.com/GArik/enlightenment-SlackBuilds
*** All credits go to him.

Credits to Ryan too, thanks for sharing
https://github.com/ryanpcmcquen/slackENLIGHTENMENT

How to install:
- you need to clone with command : git clone https://gitlab.com/grinder/enlightenment.git
And then inside the created folder run the installation script
sh build-enlightenment.sh
